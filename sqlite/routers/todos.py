from fastapi import APIRouter,Depends,HTTPException,status,Path
from pydantic import BaseModel,Field
from sqlalchemy.sql.functions import user

import models
from models import Todos
from database import engine,SessionLocal
from sqlalchemy.orm import Session
from typing import Annotated
# from routers import auth
from .auth import get_current_user


router= APIRouter(tags=['todos'])

# models.Base.metadata.create_all(bind=engine)

# router.include_router(auth.router)

def get_db():
    db=SessionLocal()
    try:
        yield db
    finally:
        db.close()

db_dependecy=Annotated[Session,Depends(get_db)]

user_dependecy= Annotated[dict,Depends(get_current_user)]

class TodoRequest(BaseModel):
    title:str=Field(min_length=3)
    description:str=Field(min_length=3,max_length=100)
    priority:int = Field(gt=0,lt=6)
    complete:bool


@router.get('/')
async def read_all(user:user_dependecy,db:db_dependecy):
    # buscar tareas por id del usuario
    # return db.query(Todos).filter().all()

    if user is None:
        raise HTTPException(status_code=401, detail='Authentication Failed')

    return db.query(Todos).filter(Todos.owner_id==user.get('id')).all()




@router.get('/todo/{todo_id}',status_code=status.HTTP_200_OK)
async def read_todo(user:user_dependecy,db: db_dependecy,todo_id:int=Path(gt=0)):
    if user is None:
        raise HTTPException(status_code=401, detail='Authentication Failed')

    # todo_model= db.query(Todos).filter(Todos.id==todo_id).first()
    todo_model = db.query(Todos).filter(Todos.id == todo_id).filter(Todos.owner_id==user.get('id')).first()

    if todo_model is not None:

        return todo_model
    raise HTTPException(status_code=404, detail='Not Found')




@router.post('/todo',status_code=status.HTTP_201_CREATED)
async def create_todo(user:user_dependecy,db:db_dependecy,todo_request:TodoRequest):

    if user is None:
        raise HTTPException(status_code=401, detail='Authentication Failed')
    # se agrega el campo owner_id a la tarea
    # todo_model = Todos(**todo_request.model_dump())
    todo_model=Todos(**todo_request.model_dump(),owner_id=user.get('id'))

    db.add(todo_model)
    db.commit()




@router.put('/todo/{todo_id}',status_code=status.HTTP_204_NO_CONTENT)
async def update_todo(user:user_dependecy,db:db_dependecy, todo_request:TodoRequest, todo_id:int=Path(gt=0)):

    if user is None:
        raise HTTPException(status_code=401, detail='Authentication Failed')
    # todo_model= db.query(Todos).filter(Todos.id==todo_id).first()
    todo_model = db.query(Todos).filter(Todos.id == todo_id).filter(Todos.owner_id==user.get('id')).first()


    if todo_model is None:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail='Todo not found')

    todo_model.title= todo_request.title
    todo_model.description= todo_request.description
    todo_model.priority= todo_request.priority
    todo_model.complete= todo_request.complete

    db.add(todo_model)
    db.commit()

@router.delete('/todo/{todo_id}',status_code=status.HTTP_204_NO_CONTENT)
async def delete_todo(user:user_dependecy,db:db_dependecy,todo_id:int =Path(gt=0)):
    if user is None:
        raise HTTPException(status_code=401, detail='Authentication Failed')
    # todo_model= db.query(Todos).filter(Todos.id==todo_id).first()
    todo_model = db.query(Todos).filter(Todos.id == todo_id).filter(Todos.owner_id==user.get('id')).first()
    if todo_model is None:
        raise HTTPException(status_code=404, detail='Todo not found')
    db.query(Todos).filter(Todos.id==todo_id).filter(Todos.owner_id==user.get('id')).delete()
    db.commit()
