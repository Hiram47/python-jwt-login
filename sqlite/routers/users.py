from fastapi import APIRouter,Depends,HTTPException,status,Path
from pydantic import BaseModel,Field
from sqlalchemy.sql.functions import user

import models
from models import Todos,Users
from database import engine,SessionLocal
from sqlalchemy.orm import Session
from typing import Annotated
# from routers import auth
from .auth import get_current_user
from passlib.context import CryptContext




router= APIRouter(
    prefix='/user',
    tags=['user']
)

# models.Base.metadata.create_all(bind=engine)

# router.include_router(auth.router)


def get_db():
    db=SessionLocal()
    try:
        yield db
    finally:
        db.close()

db_dependecy=Annotated[Session,Depends(get_db)]

user_dependecy= Annotated[dict,Depends(get_current_user)]

bcrypt_context= CryptContext(schemes=['bcrypt'],deprecated='auto')

class UserVerification(BaseModel):
    password:str
    new_password:str=Field(min_length=6)

@router.get('/',status_code=status.HTTP_200_OK)
async def get_user(user:user_dependecy,db:db_dependecy):
    if user is None:
        raise HTTPException(status_code=status.HTTP_404_NOT_FOUND, detail='Autherization Failed')
    return db.query(Users).filter(Users.id== user.get('id')).first()

@router.put('/password',status_code=status.HTTP_204_NO_CONTENT)
async def change_password(user:user_dependecy,db:db_dependecy,user_verificarion:UserVerification):
    if user is None:
        raise HTTPException(status_code=401,detail='Authentication Failed')

    user_model= db.query(Users).filter(Users.id==user.get('id')).first()

    if not bcrypt_context.verify(user_verificarion.password,user_model.hashed_password):
        raise HTTPException(status_code=401,detail='Error on password change')

    user_model.hashed_password= bcrypt_context.hash(user_verificarion.new_password)
    db.add(user_model)
    db.commit()



